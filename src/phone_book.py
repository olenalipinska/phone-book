from Controller import Controller
from models.CustomError import CustomError
from helpers.Validators import *
phone_book = Controller()

while True:
    print('1 - Show all contacts\n2 - Add new contact\n3 - Delete contact\n4 - Edit contact\n5 - Search\n6 - Sort '
          'records\n7 - Exit\n')

    choice = input('Select the option: ')

    match choice:
        case '1':
            phone_book.show_all_contacts()

        case '2':

            while True:
                first_name = input('First name*: ')
                if is_empty(first_name):
                    print('Field is required')
                else:
                    break
            while True:
                second_name = input('Second name*: ')
                if is_empty(second_name):
                    print('Field is required')
                else:
                    break
            while True:
                phone = input('Phone*: ')
                if is_phone_number_valid(phone):
                    break
                else:
                    print('Please enter correct value: \n(ex. +380555555555)')

            address = input('Address: ')

            while True:
                birth_date = input('Birth date (DD.MM.YYYY): ')
                if is_empty(birth_date):
                    break
                else:
                    if is_birth_date_valid(birth_date):
                        break
                    else:
                        print('Please enter correct value: \n(DD.MM.YYYY)')

            phone_book.add_new_contact(first_name, second_name, phone, address, birth_date)

        case '3':
            try:
                contact_to_remove = int(input('Select ID: '))
                try:
                    phone_book.delete_contact(contact_to_remove)
                except CustomError as e:
                    print(e)
            except ValueError:
                print('Enter correct value')

        case '4':
            try:
                to_edit = int(input('Select ID: '))
                try:
                    phone_book.edit_contact(to_edit)
                except CustomError as e:
                    print(e)
            except ValueError:
                print('Enter correct value')

        case '5':
            search_parameter = input('Enter name or phone number: ')
            phone_book.search_contact(search_parameter)

        case '6':
            print('1 - Sort by First name\n2 - Sort by Second name\n')
            value_for_sort = input('Select type of sort: ')
            match value_for_sort:
                case '1':
                    phone_book.change_sort_method('first_name')
                case '2':
                    phone_book.change_sort_method('second_name')

        case '7':
            break

