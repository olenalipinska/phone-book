import csv
import os.path
from models.CustomError import CustomError
from helpers.Validators import *
from models.Contact import Contact


class SingletonMeta(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            instance = super().__call__(*args, **kwargs)
            cls._instances[cls] = instance
        return cls._instances[cls]


class Controller(metaclass=SingletonMeta):
    def __init__(self):
        self.contacts = self.get_contacts_from_file('data/contacts.csv')
        self.sorted_by = self.get_sort_config_from_file('data/sort_config.csv')

    @staticmethod
    def get_sort_config_from_file(file):
        if os.path.isfile(file):
            with open(file, 'rt') as csvfile:
                reader = csv.reader(csvfile)
                try:
                    for line in reader:
                        if line[0] == 'first_name' or line[0] == 'second_name':
                            return line[0]
                        else:
                            return 'first_name'
                except IndexError:
                    return 'first_name'
        else:
            return 'first_name'

    def write_sort_config_to_file(self):
        with open('data/sort_config.csv', 'wt') as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow([self.sorted_by])

    def show_all_contacts(self):
        for contact in self.contacts:
            print(contact)

    def delete_contact(self, chosen_idx):
        to_remove = next((x for x in self.contacts if x.uid == chosen_idx), None)
        if to_remove is not None:
            self.contacts.remove(to_remove)
            self.write_contacts_to_file()
            print(f'Contact {to_remove} has been deleted')
        else:
            raise CustomError('There is no contact with such ID')

    def edit_contact(self, chosen_idx):
        contact_to_edit = next((x for x in self.contacts if x.uid == chosen_idx), None)
        if contact_to_edit is not None:
            while True:
                print('Press 0 to skip')
                first_name = input('New First name: ')
                if first_name == '0':
                    break
                if is_empty(first_name):
                    print('Field is required')
                else:
                    contact_to_edit.first_name = first_name
                    break
            while True:
                print('Press 0 to skip')
                second_name = input('New Second name: ')
                if second_name == '0':
                    break
                if is_empty(second_name):
                    print('Field is required')
                else:
                    contact_to_edit.second_name = second_name
                    break
            while True:
                print('Press 0 to skip')
                phone = input('New Phone: ')
                if phone == '0':
                    break
                if is_phone_number_valid(phone):
                    contact_to_edit.phone = phone
                    break
                else:
                    print('Please enter correct value: \n(ex. +380555555555)')
            print('Press 0 to skip')
            address = input('New Address: ')
            if address == '0':
                pass
            else:
                contact_to_edit.address = address
            while True:
                print('Press 0 to skip')
                birth_date = input('New Birth date (DD.MM.YYYY): ')
                if birth_date == '0':
                    break
                if is_birth_date_valid(birth_date):
                    contact_to_edit.birth_date = birth_date
                    break
                else:
                    print('Please enter correct value: \n(DD.MM.YYYY)')
        else:
            raise CustomError('There is no contact with such ID')
        self.write_contacts_to_file()
        print(f'Contact has been edited! New contact is: {contact_to_edit}')

    def search_contact(self, chosen_value):
        search_result = []
        for contact in self.contacts:
            if contact.first_name.lower().startswith(chosen_value.lower())\
                    or contact.second_name.lower().startswith(chosen_value.lower()):
                search_result.append(contact)
                print(contact)
            elif contact.phone.startswith(chosen_value):
                search_result.append(contact)
                print(contact)
        if len(search_result) == 0:
            print('Contact is not found')

    @staticmethod
    def get_contacts_from_file(file):
        contacts = []
        if os.path.isfile(file):
            with open(file, 'rt') as csvfile:
                contact_reader = csv.reader(csvfile, delimiter=',')
                for line in contact_reader:
                    contacts.append(Contact(int(line[0]), line[1], line[2], line[3], line[4], line[5]))
                print(f'Total number of contacts: {len(contacts)}')
                return contacts
        else:
            print(f'Total number of contacts: {len(contacts)}')
            return contacts

    def add_new_contact(self, first_name, second_name, phone, address, birth_date):
        self.contacts.append(Contact(len(self.contacts)+1, first_name, second_name, phone, address, birth_date))
        self.sort_contacts()
        print(f'Contact {first_name} {second_name} has been added!')

    def write_contacts_to_file(self):
        with open('data/contacts.csv', 'wt') as csvfile:
            contact_writer = csv.writer(csvfile, delimiter=',')
            for line in self.contacts:
                contact_writer.writerow([line.uid, line.first_name, line.second_name, line.phone, line.address, line.birth_date])

    def sort_contacts(self):
        if self.sorted_by == 'first_name':
            self.contacts = sorted(self.contacts, key=lambda x: x.first_name.lower())
        elif self.sorted_by == 'second_name':
            self.contacts = sorted(self.contacts, key=lambda x: x.second_name.lower())
        self.write_contacts_to_file()
        print('Contacts have been sorted')

    def change_sort_method(self, chosen_method):
        self.sorted_by = chosen_method
        self.write_sort_config_to_file()
        self.sort_contacts()


