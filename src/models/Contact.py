class Contact:
    def __init__(self, uid, first_name, second_name, phone, address, birth_date):
        self.uid = uid
        self.first_name = first_name
        self.second_name = second_name
        self.phone = phone
        self.address = address
        self.birth_date = birth_date

    def __str__(self):
        return f'{self.uid} {self.first_name} {self.second_name} {self.phone} {self.address} {self.birth_date}'
