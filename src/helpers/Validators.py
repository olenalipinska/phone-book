import re


def is_phone_number_valid(number):
    regex = '^(\+\d{1,2}\s?)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$'
    if re.match(regex, number):
        return True
    else:
        return False


def is_birth_date_valid(birth_date):
    regex = '^\s*(3[01]|[12][0-9]|0?[1-9])\.(1[012]|0?[1-9])\.((?:19|20)\d{2})\s*$'
    if re.match(regex, birth_date):
        return True
    else:
        return False


def is_empty(value):
    if len(value) > 0:
        return False
    else:
        return True

